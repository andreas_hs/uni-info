package structs

type Borders struct {
	Borders []string `json:"borders"`
}

type CombinedStruct struct {
	Name      string            `json:"name"`
	Country   string            `json:"country"`
	TwoCode   string            `json:"alpha_two_code"`
	Domains   []string          `json:"domains"`
	Languages map[string]string `json:"languages"`
	Map       string            `json:"maps"`
}

type Country struct {
	Names     Name              `json:"name"`
	Languages map[string]string `json:"languages"`
	Maps      Map               `json:"maps"`
	Cca2      string            `json:"cca2"`
}

type Map struct {
	GoogleMaps     string `json:"googleMaps"`
	OpenStreetMaps string `json:"openStreetMaps"`
}

type Name struct {
	Common      string            `json:"common"`
	Official    string            `json:"official"`
	NativeNames map[string]string `json:"native_names"`
}

type DiagStruct struct {
	Universitiesapi string  `json:"universitiesapi"`
	Countriesapi    string  `json:"countriesapi"`
	Version         string  `json:"version"`
	Uptime          float64 `json:"uptime"`
}

type University struct {
	WebPages []string `json:"web_pages"`
	Name     string   `json:"name"`
	TwoCode  string   `json:"alpha_two_code"`
	Province string   `json:"state-province"`
	Domains  []string `json:"domains"`
	Country  string   `json:"country"`
}
