package main

import (
	"assignment1/handler"
	"assignment1/time"
	"log"
	"net/http"
	"os"
)

func main() {
	time.ReturnStartTime()
	// Extract PORT variable from the environment variables - used in Heroku
	port := os.Getenv("PORT")

	// Override port with default port if not provided (e.g. local deployment)
	if port == "" {
		log.Println("$PORT has not been set. Default: 8080")
		port = "8080"
	}

	http.HandleFunc(handler.INDEX_PATH, handler.IndexHandler)

	http.HandleFunc(handler.UNI_INFOPATH, handler.InfoHandler)

	http.HandleFunc(handler.DIAG_PATH, handler.DiagHandler)

	http.HandleFunc(handler.NEIGHBOUR_UNIS_PATH, handler.NeighbourHandler)

	// Start HTTP server
	log.Println("Starting server on port " + port + " ...")
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
