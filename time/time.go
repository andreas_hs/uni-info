package time

import "time"

var StartTime = time.Now()

// when method is called startTime is initialized
func ReturnStartTime() time.Time {
	return StartTime
}

// return the time since startTime was initialized
func TimeSinceStart() float64 {
	return time.Since(StartTime).Seconds()
}
