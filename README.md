# Assignment-1

- Author: Andreas Sellesbakk

## Description
This project is a restAPI written in golang 1.17

This goal of this project is to give you a way to enter a search-word, get information about universities, and information about the country the university is in.
It also gives you a way to search for universities in a specific country, or a country and its neighbours.

The goals of this project:
- be able to search for universities using a search-word, and get information about the univerisities and the countries they are located in
- be able to search for a country and a searchword, and get information about universities and the countries they are located in, in the country you searched for and its bordering countries

##How to clone:

open git bash

locate where you want this project to be

then simply type git clone https://git.gvk.idi.ntnu.no/course/prog2005/prog2005-2022-workspace/andreas__/assignment1.git

## How to use:

### indexpage
if you just type http://localhost:8080/ you will go to an indexpage where you get some basic information about how to use this project



### uniinfo

- http://localhost:8080/unisearcher/v1/uniinfo

To use uniinfo you write the url given above, and then write /{searchWord}

For example:

http://localhost:8080/unisearcher/v1/uniinfo/science

This will give you all the universities with science in the name

You can also search in a specific country by adding &country={countryName}

This will give you all the universities in norway with science in the name


For example:

http://localhost:8080/unisearcher/v1/uniinfo/science&country=norway
### diag
- http://localhost:8080/unisearcher/v1/diag/


  The diagnostic function is simple to use, just write the url given above.

 This will give you the status code for the two apis used, and the uptime of this code.

### neighbourHandler

- http://localhost:8080/unisearcher/v1/neighbourunis/

To use neighbourunis use the url as given bellow, note that limit is optional.

if limit is exclude, or is set to 0 you will get all the results.

if limit is set to a number, you will get that many resaults

http://localhost:8080/unisearcher/v1/neighbourunis/{searchCountry}}/{searchWord}}?limit={number}

For example:

http://localhost:8080/unisearcher/v1/neighbourunis/norway/science?limit=10

This will give you the 10 first unis from norway and its bordering countries, where science is in the name.

If you did the same thing without the limit, like this:

http://localhost:8080/unisearcher/v1/neighbourunis/norway/science

You will get ALL the unis from norway and its bordering

