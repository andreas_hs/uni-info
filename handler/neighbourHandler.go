package handler

import (
	"assignment1/structs"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

func NeighbourHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		http.Error(w, "method not supported.", http.StatusNotImplemented)
	case http.MethodGet:
		HandlerNeighbourRequest(w, r)
	default:
		http.Error(w, "method not supported.", http.StatusNotImplemented)
		return
	}
}

func HandlerNeighbourRequest(w http.ResponseWriter, r *http.Request) {
	// splits up the path
	parts := strings.Split(r.URL.Path, "/")
	//checks if it is the right amount of arguments in the url
	if len(parts) < 6 || len(parts[5]) == 0 {
		http.Error(w, " missing arguments", http.StatusBadRequest)
		return
	}
	//checks if it is the right amount of arguments in the url
	if len(parts) > 6 && len(parts[6]) != 0 {
		http.Error(w, " to many arguments", http.StatusBadRequest)
		return
	}
	//replaces spaces with %20
	countrySearch := strings.ReplaceAll(parts[4], " ", "%20")
	uniSearch := strings.ReplaceAll(parts[5], " ", "%20")

	var countriesToSearch []structs.Country
	//add the country you searched for first to the list
	countriesToSearch = append(countriesToSearch, getCountryInfo(w, countrySearch)...)
	// add the bordering countries to the same list
	countriesToSearch = append(countriesToSearch, BorderingCountries(w, getBorders(w, countrySearch))...)

	var limitAsInt int
	var err error

	//checks if limit is given, if so it converts it to an int
	if len(r.URL.RawQuery) > 0 {
		partsRawQuery := strings.Split(r.URL.RawQuery, "=")
		limitAsString := partsRawQuery[1]
		limitAsInt, err = strconv.Atoi(limitAsString)
		if err != nil {
			http.Error(w, "please give limit as a number", http.StatusBadRequest)
			return
		}
	} else {
		// if limit is not given then it will be set to 0
		limitAsInt = 0
	}

	// gets all the universities with the search-word
	var uniList = getUniversity(w, uniSearch)

	//makes a list to respond with
	var responseList = createResponseList(countriesToSearch, uniList)

	// if limit is set to 0 you get all the responses
	if limitAsInt == 0 {
		limitAsInt = len(responseList)
	}

	// if the length of the list is longer then the limit the list is made to be the length of the limit
	if len(responseList) >= limitAsInt {
		responseList = append(responseList[:limitAsInt], responseList[len(responseList):]...)
	}

	replyCombinedData(w, responseList)

}

// finds the borders to a country (as codes)
func getBorders(w http.ResponseWriter, countryString string) []structs.Borders {
	// this is the first part of the api link
	link1 := "https://restcountries.com/v3.1/name/"
	//this is the second part of the api link where we specify the fields we want
	link2 := "?fields=borders"
	var stringToSend strings.Builder
	// first part of link
	stringToSend.WriteString(link1)
	//country we want info about
	stringToSend.WriteString(countryString)
	//second part of link
	stringToSend.WriteString(link2)
	//gets the information from the api
	resp, err := http.Get(stringToSend.String())

	//error handling
	if err != nil {
		http.Error(w, "Bad Gateway", http.StatusBadGateway)
		return nil
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			http.Error(w, "Internal server error.", http.StatusInternalServerError)
			return
		}
	}(resp.Body)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, "Internal server error.", http.StatusInternalServerError)
		return nil
	}

	var response []structs.Borders
	err = json.Unmarshal(body, &response)

	if err != nil {
		http.Error(w, "Internal server error.", http.StatusInternalServerError)
		return nil
	}
	// returns the response from the api
	return response
}

// BorderingCountries turns the aplha2codes to countries
func BorderingCountries(w http.ResponseWriter, borders []structs.Borders) []structs.Country {
	var borderingCountries []structs.Country

	for j := range borders {
		for i := range borders[j].Borders {
			borderingCountries = append(borderingCountries, findCountryUsingAlpha(w, borders[j].Borders[i]))
		}
	}

	return borderingCountries
}

//uses alphacode to fund country
func findCountryUsingAlpha(w http.ResponseWriter, alphaCode string) structs.Country {
	link1 := "https://restcountries.com/v3.1/alpha/"
	link2 := "?fields=name,languages,maps,cca2"
	var stringToSend strings.Builder
	stringToSend.WriteString(link1)
	stringToSend.WriteString(alphaCode)
	stringToSend.WriteString(link2)
	//gets the information from the api
	resp, err := http.Get(stringToSend.String())

	//error handling
	if err != nil {
		http.Error(w, "Bad Gateway", http.StatusBadGateway)
		return structs.Country{}
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			http.Error(w, "Internal server error.", http.StatusInternalServerError)
			return
		}
	}(resp.Body)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, "Internal server error.", http.StatusInternalServerError)
		return structs.Country{}
	}

	var response structs.Country
	err = json.Unmarshal(body, &response)

	if err != nil {
		http.Error(w, "Internal server error.", http.StatusInternalServerError)
		return structs.Country{}
	}
	// returns the response from the api
	return response

}
