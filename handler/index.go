package handler

import (
	"fmt"
	"net/http"
)

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		http.Error(w, "method not supported.", http.StatusNotImplemented)
	case http.MethodGet:
		indexGetRequest(w, r)
	default:
		http.Error(w, "method not supported.", http.StatusNotImplemented)
		return
	}

}

// makes strings with info about the endpoints, and how to use them.
func indexGetRequest(w http.ResponseWriter, r *http.Request) {
	var endpoints string
	var usage string
	endpoints = "--endpoints--" + "\n"
	endpoints = endpoints + "unisearcher/v1/uniinfo/" + "\n"
	endpoints = endpoints + "unisearcher/v1/neighbourunis/" + "\n"
	endpoints = endpoints + "unisearcher/v1/diag/" + "\n"

	usage = "\n" + "--usage--" + "\n"
	usage = usage + "unisearcher/v1/uniinfo/{search-word}" + "\n"
	usage = usage + "unisearcher/v1/neighbourunis/{country}/{search-word}?limit={number}" + "\n"
	usage = usage + "unisearcher/v1/diag/" + "\n"

	_, err := fmt.Fprintf(w, endpoints+usage)
	if err != nil {
		http.Error(w, "bad gateway", http.StatusBadRequest)
		return
	}
}
