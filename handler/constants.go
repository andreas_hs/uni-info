package handler

const VERSION = "v1"

const INDEX_PATH = "/"

const UNI_INFOPATH = "/unisearcher/" + VERSION + "/uniinfo/"

const DIAG_PATH = "/unisearcher/" + VERSION + "/diag/"

const NEIGHBOUR_UNIS_PATH = "/unisearcher/" + VERSION + "/neighbourunis/"
