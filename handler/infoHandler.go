package handler

import (
	"assignment1/structs"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
)

func InfoHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		http.Error(w, "method not supported.", http.StatusNotImplemented)
	case http.MethodGet:
		handleUniversityGetRequest(w, r, 0)
	default:
		http.Error(w, "method not supported.", http.StatusNotImplemented)
		return
	}

}

func handleUniversityGetRequest(w http.ResponseWriter, r *http.Request, limit int) {
	parts := strings.Split(r.URL.Path, "/")
	//checks if the parts are the correct length
	if len(parts) < 5 || len(parts[4]) == 0 {
		http.Error(w, " missing arguments", http.StatusBadRequest)
		return
	}
	if len(parts) > 5 && len(parts[5]) != 0 {
		http.Error(w, " to many arguments", http.StatusBadRequest)
		return
	}
	//replaces all the spaces with %20
	nameToSend := strings.ReplaceAll(parts[4], " ", "%20")
	//makes a list of all the unis with the search-word
	var universityList = getUniversity(w, nameToSend)
	if universityList == nil || len(universityList) == 0 {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}
	var countryList []structs.Country
	// adds the first country from the uniList to the countryList
	countryList = append(countryList, getCountryInfo(w, universityList[0].Country)...)

	//adds one of each country from the uniList
	countryList = checkForCountryInList(w, countryList, universityList)

	//makes a list with the combined info
	responseList := createResponseList(countryList, universityList)

	if limit == 0 || limit >= len(responseList) {
		limit = len(responseList)
	}
	// if the list is longer then the limit then the list will be appended to be the correct length
	if len(responseList) >= limit {

		responseList = append(responseList[:limit], responseList[len(responseList):]...)
	}
	replyCombinedData(w, responseList)

}

func replyCombinedData(w http.ResponseWriter, responseListToSend []structs.CombinedStruct) {
	if len(responseListToSend) == 0 {
		http.Error(w, "No matches to your search", http.StatusBadRequest)
		return
	}
	w.Header().Add("content-type", "application/json")
	encoder := json.NewEncoder(w)
	encoder.SetIndent("", "\t")
	err := encoder.Encode(responseListToSend)
	if err != nil {
		http.Error(w, "ERROR encoding.", http.StatusInternalServerError)
		return
	}
}

// combines the data about the university, and the data about the countries using two forloops.
//matches the alpha2code from the university data and the code from country data.
func createResponseList(countryInfo []structs.Country, uniInfo []structs.University) []structs.CombinedStruct {
	var responseList []structs.CombinedStruct
	for i := range uniInfo {
		for j := range countryInfo {
			if uniInfo[i].TwoCode == countryInfo[j].Cca2 {
				responseList = append(responseList, structs.CombinedStruct{
					Name:      uniInfo[i].Name,
					Country:   uniInfo[i].Country,
					TwoCode:   uniInfo[i].TwoCode,
					Domains:   uniInfo[i].Domains,
					Languages: countryInfo[j].Languages,
					Map:       countryInfo[j].Maps.OpenStreetMaps,
				})
			}
		}
	}

	return responseList

}

// checks if a country is already in the list, if its not adds it to the list
func checkForCountryInList(w http.ResponseWriter, countryList []structs.Country, universityList []structs.University) []structs.Country {
	var flag = false
	for i := range universityList {
		flag = false
		for j := range countryList {
			if universityList[i].TwoCode == countryList[j].Cca2 {
				j = len(countryList)
				flag = true

			}
		}
		if !flag {
			countryList = append(countryList, getCountryInfo(w, universityList[i].Country)...)
		}
	}
	return countryList
}

//gets the university with the search word
func getUniversity(w http.ResponseWriter, searchString string) []structs.University {
	searchString = strings.ReplaceAll(searchString, " ", "%20")

	// this is the api link
	link := "http://universities.hipolabs.com/search?name_contains="
	var stringToSend strings.Builder
	//api link
	stringToSend.WriteString(link)
	//your search
	stringToSend.WriteString(searchString)

	//gets the information from the api
	resp, err := http.Get(stringToSend.String())
	//error handling
	if resp == nil {
		http.Error(w, "Bad Gateway", http.StatusBadGateway)
		return nil
	}
	if err != nil {
		http.Error(w, "Bad Gateway", http.StatusBadGateway)
		return nil
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
	}(resp.Body)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return nil
	}

	var response []structs.University
	err = json.Unmarshal(body, &response)

	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return nil
	}
	// returns the response from the api

	return response
}

// gets information about the country we searched for
func getCountryInfo(w http.ResponseWriter, countryString string) []structs.Country {
	// this is the first part of the api link
	link1 := "https://restcountries.com/v3.1/name/"
	//this is the second part of the api link where we specify the fields we want
	link2 := "?fields=name,languages,maps,cca2"
	var stringToSend strings.Builder

	// first part of link
	stringToSend.WriteString(link1)
	//country we want info about
	stringToSend.WriteString(countryString)
	//second part of link
	stringToSend.WriteString(link2)

	//gets the information from the api
	resp, err := http.Get(stringToSend.String())
	if resp == nil {
		http.Error(w, "Bad Gateway", http.StatusBadGateway)
		return nil
	}
	if err != nil {
		http.Error(w, "Bad Gateway", http.StatusBadGateway)
		return nil
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
	}(resp.Body)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return nil
	}

	var response []structs.Country
	err = json.Unmarshal(body, &response)

	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return nil
	}
	// returns the response from the api
	return response
}
