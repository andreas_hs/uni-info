package handler

import (
	"assignment1/structs"
	"assignment1/time"
	"encoding/json"
	"fmt"
	"net/http"
)

func DiagHandler(w http.ResponseWriter, r *http.Request) {

	var uniReply = IsUniAPIup(w)
	var countryReply = IsCountryAPIup(w)

	//handler.CombineISapiUpData(uniReply, countryReply, TimeSinceStart())
	ReplyWithStatus(w, CombineISapiUpData(uniReply, countryReply, time.TimeSinceStart()))
}

// CombineISapiUpData combines the data from the different methods to a new struct
func CombineISapiUpData(uniReply string, countryReply string, time float64) structs.DiagStruct {

	combinedResponse := structs.DiagStruct{
		Universitiesapi: uniReply,
		Countriesapi:    countryReply,
		Version:         VERSION,
		Uptime:          time,
	}
	return combinedResponse
}

// IsUniAPIup gets the status code from the uni api
func IsUniAPIup(w http.ResponseWriter) string {
	resp, err := http.Get("http://universities.hipolabs.com/")
	if err != nil {
		return "502 Bad Gateway"
	}
	return resp.Status
}

// gets the status code from the country api
func IsCountryAPIup(w http.ResponseWriter) string {
	resp, err := http.Get("https://restcountries.com/")
	if err != nil {
		return "502 Bad Gateway"
	}
	return resp.Status
}

func ReplyWithStatus(w http.ResponseWriter, diagStruct structs.DiagStruct) {
	w.Header().Add("content-type", "application/json")
	encoder := json.NewEncoder(w)
	encoder.SetIndent("", "\t")
	err := encoder.Encode(diagStruct)
	if err != nil {
		fmt.Println("LOG 4: ERROR", err)
	}
}
